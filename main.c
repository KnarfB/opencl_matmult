// set inlude and lib folders accordingly, 
// e.g. C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v10.2\

// see https://gist.github.com/tzutalin/51a821f15a735024f16b for detailed device info

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <CL/cl.h>

const char *program_buffer = {
	"__kernel void								\n"
	"matrixMul(__global float* A, 				\n"
	"          __global float* B, 				\n"
	"          __global float* C, 				\n"
	"          int n )							\n"
	"{											\n"
	"  											\n"
	"   int x = get_global_id(0); 				\n"
	"   int y = get_global_id(1);				\n"
	" 											\n"
	"   float value = 0;						\n"
	"   for (int k = 0; k < n; ++k)				\n"
	"   {										\n"
	"      float a = A[y * n + k];				\n"
	"      float b = B[k * n + x];				\n"
	"      value += a * b;						\n"
	"   }										\n"
	" 											\n"
	"   C[y * n + x] = value;					\n"
	"}											\n"
};

#define N 2048

float A[N*N] = { 6 };						// A[N][N]
float B[N*N] = { 7 };						// B[N][N]
float C[N*N];								// C[N][N]

void matrixMulClassic( 
	const float *__restrict A, 
	const float *__restrict B, 
	float *__restrict C, 
	int n )
{
	for (int y = 0; y < n; ++y) {
		for (int x = 0; x < n; ++x) {
			float value = 0;
			for (int k = 0; k < n; ++k) {
				float a = A[y * n + k];		// A[y][k]
				float b = B[k * n + x];		// B[k][x]
				value += a * b;
			}
			C[y * n + x] = value;			// C[y][x]
		}
	}
}

int main_C()
{
	clock_t tick = clock();
	matrixMulClassic(A, B, C, N);
	clock_t tock = clock();
	printf("CPU took: %0.3f ms\n", 1E3 * (tock - tick) / CLOCKS_PER_SEC);

	return C[0];
}

// caution: for brevity, there is no OpenCL error handling
// TODO: add OpenCL error handling

int main_OpenCL()
{
	clock_t tick = clock();

	cl_platform_id platform;
	clGetPlatformIDs(1, &platform, NULL);

	cl_device_id device;
	clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);

	cl_context context = clCreateContext(NULL, 1, &device, NULL, NULL, NULL);

	cl_program program = clCreateProgramWithSource(context, 1, &program_buffer, NULL, NULL);

	clBuildProgram(program, 0, NULL, NULL, NULL, NULL);

	cl_command_queue queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, NULL);

	cl_kernel kernel = clCreateKernel(program, "matrixMul", NULL);

	cl_mem clA = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(A), A, NULL);
	cl_mem clB = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(B), B, NULL);
	cl_mem clC = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(C), NULL, NULL);

	cl_int n = N;
	clSetKernelArg(kernel, 0, sizeof(cl_mem), (void*)&clA);
	clSetKernelArg(kernel, 1, sizeof(cl_mem), (void*)&clB);
	clSetKernelArg(kernel, 2, sizeof(cl_mem), (void*)&clC);
	clSetKernelArg(kernel, 3, sizeof(cl_int), (void*)&n);

	size_t globalWorkSize[2] = { N, N };
	
	cl_event event;
	clEnqueueNDRangeKernel(queue, kernel, 2, NULL, globalWorkSize, NULL, 0, NULL, &event );
	clWaitForEvents(1, &event);

	clEnqueueReadBuffer(queue, clC, CL_TRUE, 0, sizeof(C), C, 0, NULL, NULL);

	clFinish(queue);

	clock_t tock = clock();
	printf("OpenCL took: %0.3f ms\n", 1E3 * (tock - tick) / CLOCKS_PER_SEC);

	cl_ulong time_start;
	cl_ulong time_end;

	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);

	double nanoSeconds = time_end - time_start;
	printf("(kernel took: %0.3f ms)\n", nanoSeconds / 1E6 );

	return C[0];
}

int main()
{
	//return main_C();
	return main_OpenCL();
}